.. _settings-input-output-table:

Batteries Setting Table
=======================

.. image:: _static/linq8acm-settings-batteries.png

The Battery Settings Table is where you can configure the following features.

1. :ref:`settings-battery-installation-date`

2. :ref:`settings-battery-service-date`

3. :ref:`settings-battery-thresholds`

4. :ref:`settings-battery-presence`

.. _settings-battery-port:

Battery Port
------------

An index between 1 and 2 that represents the battery position on the LinQ8ACM circuit board

.. _settings-battery-installation-date:

Battery Installation Date
-------------------------

An indicator to display when the battery was installed. This field is present on the status page for this battery

.. _settings-battery-service-date:

Battery Service Date
--------------------

An indicator to display when the battery is required to be serviced.
An alert will be sent 1 day prior to the installation service date.

.. _settings-battery-thresholds:

Battery Voltage and Current Thresholds
--------------------------------------

The "Max Current Charge" field will program the battery to generate an alert if the current draw charging the battery exceeds the value (in Amps).

The "Max Current Discharge" field will program the battery to generate an alert if the current draw loading the battery exceeds the value (in Amps).

The "Under Voltage" field will program the battery to generate an alert if the voltage of the battery falls below specification value (in Volts).

.. _settings-battery-presence:

Battery Presence
----------------

The "Present" field will program the status page to hide the battery if the battery is not installed.



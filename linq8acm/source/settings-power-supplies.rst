Power Supply Setting Table
==========================

.. image:: _static/linq8acm-settings-power-supplies.png

The Power Supply Setting Table is where you can configure the following features.

1. :ref:`settings-power-supply-id`

2. :ref:`settings-power-supply-thresholds`

3. :ref:`settings-power-supply-present`

Power Supply Port
-----------------

An index between 1 and 2 that represents the power supply position on the LinQ8ACM circuit board

.. _settings-power-supply-id:

Power Supply ID
---------------

The ID is a user friendly name you may assign to the Power Supply.
If the power supply requires attention from an alert, the name is provided as part of the alert.

.. _settings-power-supply-thresholds:

Power Supply Voltage and Current Thresholds
-------------------------------------------
   
The "Over Current" field will program the power supply to generate an alert if the current draw loading the power supply exceeds the value (in Amps).

The "Under Current" field will program the power supply to generate an alert if the current draw loading the power supply is less than the value (in Amps).

.. _settings-power-supply-present:

Power Supply Present
--------------------

The "Present" field will program the status page to hide the power supply if the power supply is not installed.


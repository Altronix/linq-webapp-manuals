Output Status Table
===================
 
.. image:: _static/linq8acm-status-output-table.png

The Output Status Table is where you can monitor the following features.
It is located from the side panel: hardware -> status.

1. :ref:`status-output-id`

2. :ref:`status-voltage`

3. :ref:`status-current`

4. :ref:`status-message`

5. :ref:`status-on-off`

6. :ref:`status-port-control`

7. :ref:`status-output-indicators`

Output Port
-----------

The Port number is the output or power supply index that represents the output
or power supply position on the LinQ8ACM circuit board.

.. _status-output-id:

Output ID
---------

This is the user assigned name of the output channel or power supply.
You can configure the name by using the settings page :ref:`settings-output-id`

.. note::
   The ID column displays the name of the item used to generate alerts.

.. _status-voltage:

Output and Power Supply Voltage
-------------------------------

The voltage measured by the LinQ8ACM is reported in the row for the respective
output or power supply channel.  Reporting will turn blue or red to indicate
when a voltage is out of specification.

.. _status-current:

Output and Power Supply Current
-------------------------------

The current measured by the LinQ8ACM is reported on the row for the respective
output or power supply channel.  Reporting will turn blue or red to indicate
when a current is out of specification.

.. _status-message:

Output Status Message
---------------------

The "Output Status Message" is a readable string describing the state of the output.

.. table::
   :align: left

   ======================= ===========
   Status Message          Description
   ======================= ===========
   FACP                    When the output is shutdown because of FACP
   OK                      When the output is in nominal condition
   Manually Closed         When the output is controlled into the off state
   Battery Backup Disabled When the output is enabled but is shutdown because there is no AC
   ======================= ===========

.. note:: Available for output channel rows only.

.. _status-on-off:

Output On / Off
-----------------------

The On / Off indicator will represent the position of the output relay
for the following output of the same row. "Off" will mean the output relay
position is in the "Fail Secure" position and "ON" will indicate that the output
relay position is in the "Fail Safe" postion.

.. note:: This indicator is for the output channels only. 

.. _status-port-control:

Output Port Control
-------------------

The "Output Port Control" indicates if the output relay position is controlled by
the hardwired input, or if it is in manual control.

.. tip::
   When "Manual Control" is enabled for a specified output, you can control the output from the status page.
   
.. _status-output-indicators:

Output Status Indicators
------------------------

The "Output Status Indicators" provide a summary of Pass / Fail alert conditions.

.. |alert| image:: _static/alert.png

.. |check| image:: _static/check.png

.. table::
   :align: left
   :class: table-img-sm

   ========= ======= ==========
   Indicator Active  In Active
   ========= ======= ==========
   FACP      |alert| |check| 
   Reset     |alert| |check| 
   Tamper    |alert| |check| 
   ========= ======= ==========

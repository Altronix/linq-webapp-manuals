Features
========

The LinQ8ACM module provides network management to control and monitor the following features.

1. Eight (8) Programmable Outputs

   * "Fail Safe" and "Fail Secure" terminals per each output

   * "Input Mode Control" or "Manual Control" per each output

   * Live voltage and current reporting

   * Voltage and current threshold settings generate alerts when the output is out of specification

   * "Power Cycle Mode" can cycle the power off for a programmed amount of time.

   * A Timer Scheduler can turn on and turn off the output at a specific time of day or week

   * FACP override (Latching or Non Latching) can shutdown the output

   * Battery backup disable to shutdown the output when AC fails

2. Eight (8) Programmable Inputs

   * Each input can be programmed to control 1 or more outputs

3. Battery monitoring for up to two (2) batteries

   * Live voltage and current monitoring

   * Programmable voltage and current threshold settings generate alerts when the battery is out of specification

   * Battery service notification to generate an alert when the battery is about to expire or needs service

4. Power supply monitoring for up to two (2) Power Supplies

   * Live voltage and current monitoring

   * Programmable voltage and current threshold settings generate alerts when the power supply is out of specification

.. warning::

   Programmable power cycle not available in "Input Control Mode"

   Programmable timer control not available in "Input Control Mode"

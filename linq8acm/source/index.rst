.. linq-webapp-manuals documentation master file, created by
   sphinx-quickstart on Mon Feb 25 16:10:02 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

LinQ8ACM Web Application Manual
===============================

.. toctree::
   :maxdepth: 3
   :caption: Contents

   revision-changes.rst
   features.rst
   status.rst
   settings.rst

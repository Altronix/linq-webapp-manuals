Settings
========

.. toctree::
   :maxdepth: 2

   settings-input-output.rst
   settings-batteries.rst
   settings-power-supplies.rst

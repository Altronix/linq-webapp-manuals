Congestion Monitor
==================

.. image:: _static/linq8acm-status-congestion-monitor.png

The "Congestion Monitor" is located in the lower left corner of the 
status page.  The Congestion Monitor provides diagnostic information 
about the network activity to the LinQ8ACM. The following features are
indicated by the congestion monitor.

1. :ref:`status-congestion-monitor-ping`

2. :ref:`status-congestion-monitor-pending`

3. :ref:`status-congestion-monitor-loading`

.. _status-congestion-monitor-ping:

Congestion Monitor Ping
-----------------------

.. _status-congestion-monitor-pending:

Congestion Monitor Pending
--------------------------

.. _status-congestion-monitor-loading:

Congestion Monitor Loading
--------------------------


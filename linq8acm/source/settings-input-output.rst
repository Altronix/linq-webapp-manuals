Input Output Settings Table
===========================
 
.. image:: _static/linq8acm-settings-io.png

The Input Output Settings Table is where you can configure the following features.

1. :ref:`settings-output-id`

2. :ref:`settings-enabled-disabled`

3. :ref:`settings-input-matrix`

4. :ref:`settings-battery-backup`

5. :ref:`settings-thresholds`

.. _settings-output-id:

Output ID
---------

The output ID is a user friendly name you may assign to the output channel. 
If the output requires attention from an alert, the name is provided as part of the alert.

.. _settings-enabled-disabled:

Output Enabled / Disabled
-------------------------

The "Enabled" field can enable or disable the output. 
When the output is "Disabled" the "Fail Secure" terminal will have power. *
When the output is "Enabled", the "Fail Safe" terminal will have power. *

.. attention:: * this needs to be confirmed (TC)

.. warning:: The "Enabled" field will not be accessible when any "Input" is configured to control the output.
   To disable any input from controlling the output, follow the instructions per the "Input Matrix" control field.

.. _settings-input-matrix:

Input Output Matrix
-------------------

The Input Output Matrix can wire the input to control the output.
You can wire one input to control one or many outputs.
To wire the input to an output, use the drop down menu in the column for the desired output
to be controlled by the input in the same row. The following drop down menu options are provided for each output.

.. table::
   :align: left

   ==== ==========
   Menu Option
   ==== ==========
   -    The output from this menu column should ignore the input signal from this row
   NO   The output from this menu column should follow the input signal from this row
   NC   The output from this menu column should invert the input signal from this row
   ==== ==========

.. _settings-battery-backup:

Output Battery Backup
---------------------

If the "Battery Backup" field is enabled, the output will remain on when there is an AC failure.
If the "Battery Backup" field is not enabled, the output will turn off when there is an AC failure.

.. _settings-power-cycle:

Output Power Cycle
------------------

The "Power Cycle" field controls how long a power cycle will take if the "Power Cycle" command is issued.

.. warning:: The Power Cycle feature is only available in "Manual" mode.

.. _settings-thresholds:

Output Voltage and Current Thresholds
-------------------------------------

The "Over Current" field will program the output to generate an alert if the current draw execeds the number provided in in the "Over Current" field.
The value of zero (0) will turn off all "Over Current" alerts.

The "Under Current" field will program the output to generate an alert if the current draw is lower than the number provided in the "Under Current" field.
The value of zero (0) will turn off all "Under Current" alerts.

.. tip:: Add a filter to your under current alerts to filter out start up conditions.
   Navigate to Settings -> alerts to edit the "Current Threshold Low" field.

The "Under Voltage" field will program the output to generate an alert if the voltage falls out of regulation.
The output will fall out of regulation when the device powered by the terminal block exceeded the specifications

.. note:: you must click the submit button before changes will take affect.

   a "Submit Required" indicator will notify you if your settings are inactive.



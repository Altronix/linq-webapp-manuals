Recent Alerts Table
===================

.. image:: _static/linq8acm-status-alerts-table.png

The "Recent Alerts Table" will show the most recent alerts from the device.
The following features of the alert are displayed in the table.

1. :ref:`status-alert-when`

2. :ref:`status-alert-location`

3. :ref:`status-alert-who`

4. :ref:`status-alert-what`

5. :ref:`status-alert-message`

.. _status-alert-when:

Alert When
-----------

.. _status-alert-location:

Alert Location
--------------

.. _status-alert-who:

Alert Who
---------

.. _status-alert-what:

Alert What
----------
.. _status-alert-message:

Alert Message
-------------

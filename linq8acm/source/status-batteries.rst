Battery Status Table
====================

.. image:: _static/linq8acm-status-batteries-table.png

The "Battery Status Table" is where you can monitor the following features.


1. :ref:`status-battery-installation-date`

2. :ref:`status-battery-service-date`

3. :ref:`status-battery-voltage`

4. :ref:`status-battery-current`

5. :ref:`status-message`

Battery Port
------------

The port number of the battery connected to the LinQ8ACM circuit board.

.. _status-battery-installation-date:

Battery Installation Date
-------------------------

When the battery was installed.

.. _status-battery-service-date:

Battery Service Date
--------------------

When the battery needs to be serviced by a technician.

.. _status-battery-voltage:

Battery Voltage
---------------

Live reading of the battery voltage measured by the LinQ8ACM.

.. _status-battery-current:

Battery Current
---------------

Live reading of the battery current measured by the LinQ8ACM.

.. note:: (charging) or (discharging) will be appended to the voltage reading
   to indicate the battery current direction.

.. _status-battery-message:

Battery Status Message
----------------------

A readable string representing the state of the battery.

.. table::
   :align: left

   ========= =======
   indicator message
   ========= =======
   Fail      No battery is connected, or the battery has failed
   Low       Battery voltage is Low
   Normal    Battery is OK
   ========= =======

.. tip:: If you do not have a battery connected to the LinQ8ACM for the battery
   indicated by the row number, then you can inform the LinQ8ACM there is no 
   battery in the settings page for the respective battery.


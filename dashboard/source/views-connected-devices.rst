.. _views-connected-devices:

Connected Devices View
======================

A filterable and searchable view of all connected or previously connected devices. If a device is green or yellow, you can access it by clicking it in the table. If it is black, you will be prompted to delete or keep it.

.. image:: _static/connectedDevicesView.jpg

Slow Connection
---------------

.. image:: _static/badConnectionDevice.jpg

Lost Connection
---------------

.. image:: _static/offlineDevice.jpg

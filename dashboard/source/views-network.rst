.. _views-network:

Network View
============

The network view allows you to upload an TSL/SSL certificate and key and allows you
to change ports for the dashboard. You will need to restart the app for changes
to take affect. The Dashboard has a set standard port that it runs on. If you
wish to change it you can change it here. Just be careful not to assign a port
that is not available. Assigning a secure port is only permitted after uploading
an SSL key and cert pair.

.. image:: _static/networkView.jpg

Uploading TSL/SSL Key and Cert
------------------------------

Add both cert and key as .pem files and click submit. If successful, the icons above should turn green

.. image:: _static/networkViewUpload.jpg

.. _views-alerts:

Alerts View
===========

The alerts view shows all of the current alerts stored from any connected devices and previously connected devices. The list is searchable and orderable

.. image:: _static/alertsView.jpg

Secondary Menu
--------------

To access the secondary menu, select one or more devices. This menu gives you the option to download an Excel file of the devices, to refresh the list of alerts, or to delete the selected alerts.

 .. image:: _static/alertsViewSelectAll.jpg

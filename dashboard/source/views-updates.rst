.. _views-updates:

Updates View
============

Updates view is for uploading new firmware directly to a device. Select from a list of connected devices to upload to. Add the firmware and the process will start. The firmware will update in the background.

.. image:: _static/updatesView.jpg

Setup
========

* Requirements

  1. Currently the Dashboard is only compatible with Windows computers

* Download

  1. Go to [insert link] and download installer

* Install

  1. To start the installation process, open the installer

  2. The process is automated and will take a few seconds

* Initialize

  1. When first starting up the app you will be required to create an admin user

  .. image:: _static/createInitialUser.jpg

  2. The username must be more than 3 characters long

  3. The password must be more than 8 characters long

  4. Press complete to finish initialization process
  

.. _views-device:

Device View
============

Device view allows you to interact directly with a device and is unique to each product line.

.. image:: _static/deviceView.jpg

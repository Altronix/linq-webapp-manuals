.. _views-about:

About View
==========

The about view is a general stats page. Here you can find which addresses and ports the dashboard is connected to, and whether https has been activated.

  .. image:: _static/aboutView.jpg

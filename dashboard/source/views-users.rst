.. _views-users:

Users View
==========

The users page allows admin users to add new users or edit their own account. Other users can only update their own account.

.. image:: _static/usersView.jpg

Add New User
------------

Add new user is only available for Admins.

  1. To add a new user, click the round plus button

    .. image:: _static/usersViewClickAdd.jpg

  2. Add a user name and password

  3. Assign a role to the user

      .. image:: _static/usersViewUser.jpg

  4. Click submit

  5. If successful the new user should show up in the list of users

      .. image:: _static/usersViewSuccessful.jpg

Delete User
-----------

  1. Press the trashcan icon next to their name

  .. image:: _static/usersViewClickDelete.jpg

Update User
-----------

  1. To update a user, click the pen icon next to their name

    .. image:: _static/usersViewClickUpdate.jpg

  2. Enter the old password for the user

  3. Enter the new password and/or new role and hit submit
